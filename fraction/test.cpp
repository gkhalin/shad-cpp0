#include <testing.h>
#include <fraction.h>

namespace tests {

void TestConstructors() {
    {
        Fraction a(5);
        ASSERT_EQ(5, a.Numerator());
        ASSERT_EQ(1, a.Denominator());
    }
    {
        Fraction a(2, 3);
        ASSERT_EQ(2, a.Numerator());
        ASSERT_EQ(3, a.Denominator());
    }
    {
        Fraction a(21, 14);
        ASSERT_EQ(3, a.Numerator());
        ASSERT_EQ(2, a.Denominator());
    }
}

void TestPlus() {
    {
        Fraction a(1, 2);
        Fraction b(1, 4);
        Fraction expected(3, 4);
        ASSERT_EQ(true, expected == a + b);
    }
    {
        Fraction a(1, 4);
        Fraction b(1);
        Fraction expected(5, 4);
        a += b;
        ASSERT_EQ(true, a == expected);
    }
}

void TestMultiply() {
    {
        Fraction a(3, 7);
        Fraction b(4, 9);
        Fraction expected(4, 21);
        ASSERT_EQ(true, expected == a * b);
    }
    {
        Fraction a(6, 7);
        Fraction b(3);
        a *= b;
        Fraction expected(18, 7);
        ASSERT_EQ(true, expected == a);
    }
}

void TestAll() {
    StartTesting();
    RUN_TEST(TestConstructors);
    RUN_TEST(TestPlus);
    RUN_TEST(TestMultiply);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
