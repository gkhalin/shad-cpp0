#include <testing.h>
#include <util.h>
#include <strict_iterator.h>
#include <find_last.h>

#include <string>
#include <vector>
#include <unordered_map>

namespace tests {

struct Int {
    Int() {}
    Int(int x): x(x) {}
    int x;

    bool operator==(const Int& r) const {
        return x == r.x;
    }
};

void Simple() {
    std::vector<std::string> data{"aba", "caba", "aba"};
    auto first = MakeStrict(data.begin(), data.begin(), data.end());
    auto last = MakeStrict(data.begin(), data.end(), data.end());
    auto it = FindLast(first, last, "aba");

    auto expected = MakeStrict(data.begin(), data.begin() + 2, data.end());
    ASSERT_EQ(true, expected == it);
}

void Empty() {
    std::vector<std::string> data;
    auto first = MakeStrict(data.begin(), data.begin(), data.end());
    auto last = MakeStrict(data.begin(), data.end(), data.end());
    ASSERT_EQ(true, first == FindLast(first, last, ""));
}

void Small() {
    std::vector<int> one{1};
    auto first = MakeStrict(one.begin(), one.begin(), one.end());
    auto last = MakeStrict(one.begin(), one.end(), one.end());

    ASSERT_EQ(true, first == FindLast(first, last, 1));
    ASSERT_EQ(true, last == FindLast(first, last, 0));
}

void EqualOnly() {
    std::vector<Int> elems{Int(1), Int(2), Int(1), Int(3)};
    auto first = MakeStrict(elems.begin(), elems.begin(), elems.end());
    auto last = MakeStrict(elems.begin(), elems.end(), elems.end());

    auto expected = MakeStrict(elems.begin(), elems.begin() + 2, elems.end());
    ASSERT_EQ(true, expected == FindLast(first, last, Int(1)));
}

void Long() {
    RandomGenerator rnd(85475);
    const int count = 1e5;
    const int val = 100;
    std::vector<int> elems(count);
    std::unordered_map<int, int> last_positions;
    for (int i = 0; i < count; ++i) {
        elems[i] = rnd.GenInt(-val, val);
        last_positions[elems[i]] = i;
    }
    auto first = MakeStrict(elems.begin(), elems.begin(), elems.end());
    auto last = MakeStrict(elems.begin(), elems.end(), elems.end());

    for (int i = -val; i <= val; ++i) {
        auto it = FindLast(first, last, i);
        auto map_it = last_positions.find(i);
        auto distance = std::distance(first, it);
        ASSERT_EQ(true, (it == last && map_it == last_positions.end()) || distance == map_it->second);
    }
}

void TestAll() {
    StartTesting();
    RUN_TEST(Simple);
    RUN_TEST(Empty);
    RUN_TEST(Small);
    RUN_TEST(EqualOnly);
    RUN_TEST(Long);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
