#include <testing.h>
#include <sort_students.h>

namespace tests {

void TestAll() {
    StartTesting();
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
