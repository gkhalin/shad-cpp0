#pragma once

#include <experimental/filesystem>
#include <experimental/optional>
#include <fstream>
#include <boost/algorithm/searching/boyer_moore.hpp>
#include <iostream>

namespace fs = std::experimental::filesystem;
using std::experimental::optional;
using boost::algorithm::boyer_moore;

struct GrepOptions {
    optional<size_t> look_ahead_length;
    size_t max_matches_per_line;

    GrepOptions() {
        max_matches_per_line = 10;
    }

    GrepOptions(size_t look_ahead_length): GrepOptions() {
        this->look_ahead_length = look_ahead_length;
    }

    GrepOptions(optional<size_t> look_ahead_length, size_t max_matches_per_line) {
        this->look_ahead_length = look_ahead_length;
        this->max_matches_per_line = max_matches_per_line;
    }
};

template<class Visitor>
void Grep(const std::string& path, const std::string& pattern, Visitor visitor,
          const GrepOptions& options) {
}
