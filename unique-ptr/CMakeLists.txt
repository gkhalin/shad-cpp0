cmake_minimum_required(VERSION 2.8)
project(unique_ptr)

if (TEST_SOLUTION)
  include_directories(../private/unique-ptr)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_unique_ptr
  test.cpp
  ../commons/catch_main.cpp)
