#include <catch.hpp>

#include <range.h>

#include <vector>

TEST_CASE("Simple") {
    REQUIRE(std::vector<int>{2, 3, 4} == Range(2, 5, 1));
    REQUIRE(std::vector<int>{1, 3} == Range(1, 5, 2));
    REQUIRE(std::vector<int>{-9, -4, 1, 6} == Range(-9, 10, 5));
}

TEST_CASE("SimpleReverse") {
    REQUIRE(std::vector<int>{5, 4, 3} == Range(5, 2, -1));
    REQUIRE(std::vector<int>{5, 3} == Range(5, 1, -2));
    REQUIRE(std::vector<int>{7} == Range(7, 6, -3));
}

TEST_CASE("Empty") {
    REQUIRE(Range(0, 0, 2).empty());
    REQUIRE(Range(2, 2, 1).empty());
    REQUIRE(Range(10, 10, -2).empty());
}

TEST_CASE("EmptyInf") {
    REQUIRE(Range(3, 7, -1).empty());
    REQUIRE(Range(3, 4, 0).empty());
    REQUIRE(Range(5, -5, 2).empty());
    REQUIRE(Range(3, -7, 0).empty());
}
