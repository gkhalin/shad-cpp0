cmake_minimum_required(VERSION 2.8)
project(dedup)

if (TEST_SOLUTION)
  include_directories(../private/dedup)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_dedup
  test.cpp
  ../commons/catch_main.cpp)
