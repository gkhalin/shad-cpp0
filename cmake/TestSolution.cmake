option(TEST_SOLUTION "Build solution" OFF)
option(ENABLE_PRIVATE_TESTS "Enable private tests" OFF)

function(patch_include_directories TARGET)
  if (TEST_SOLUTION)
    get_filename_component(TASK_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
    target_include_directories(${TARGET}
      PRIVATE ../private/${TASK_NAME})
  else()
    target_include_directories(${TARGET}
      PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
  endif()
endfunction()

function(prepend VAR PREFIX)
  set(LIST_VAR "")
  foreach(ELEM ${ARGN})
    list(APPEND LIST_VAR "${PREFIX}/${ELEM}")
  endforeach()
  set(${VAR} "${LIST_VAR}" PARENT_SCOPE)
endfunction()

function(add_shad_executable NAME)
  set(MULTI_VALUE_ARGS PRIVATE_TESTS)
  cmake_parse_arguments(SHAD_LIBRARY "" "" "${MULTI_VALUE_ARGS}" ${ARGN})

  if (ENABLE_PRIVATE_TESTS)
    get_filename_component(TASK_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
    prepend(SHAD_LIBRARY_PRIVATE_TESTS "../private/${TASK_NAME}" ${SHAD_LIBRARY_PRIVATE_TESTS})

    add_executable(${NAME}
      ${SHAD_LIBRARY_UNPARSED_ARGUMENTS}
      ${SHAD_LIBRARY_PRIVATE_TESTS})
  else()
    add_executable(${NAME} ${SHAD_LIBRARY_UNPARSED_ARGUMENTS})
  endif()
endfunction()

add_custom_target(test-all)

function(add_catch TARGET)
  add_shad_executable(${TARGET}
    ${ARGN})

  target_link_libraries(${TARGET}
    contrib_catch_main)

  patch_include_directories(${TARGET})

  if (TEST_SOLUTION)
    add_custom_target(
      run_${TARGET}
      WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
      DEPENDS ${TARGET}
      COMMAND ./${TARGET})

    add_dependencies(test-all run_${TARGET})
  endif()
endfunction()
