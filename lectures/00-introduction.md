# Лекция 1. Введение в C++

Фёдор Короткий

---

# О чем этот курс?

 * Современный С++ (Modern C++)
 * Как _правильно_ использовать возможности языка
 * Популярные библиотеки и полезные паттерны
 * Инструменты
 * Подходы к разработке

---

# Задания в курсе

 - 12+ семинаров
 - 3 домашних задания
 - 1-3 Code Review (кода из семинаров или домашних заданий)

---

# Критерии оценки

 - **Зачёт**: 2 сданных домашних задания + 50% за семинары
 - **Хорошо**: 2 дз + 70% или 3 дз + 60%
 - **Отлично**: 2 дз + 90% или 3 дз + 80%

---

# Телеграм чат

 - Есть у вас есть вопрос или возникла какая-то проблемма - Задавайте вопрос в чате.
 - Флуд не приветствуется
 - Логи, вывод компилятора и большие куски кода заливайте на gist.github.com

---

# План на сегодня

 1. Немного истории
 2. Hello World, синтаксис, базовые типы.
 3. Сборка, запуск, настройка окружения.
 4. Концепция _Zero-Cost Abstraction_

---

# Кто это?

![](00-introduction/straustrup.jpg)

---

# История стандартов C++

## с++98
 **С++**, на котором писал начальник вашего начальника

## с++11
 Добавил очень много важных фичей. Язык сильно улучшился, но остался обратно совместим.

## с++14
 Небольшие правки. Можно назвать `c++11.1`.

## с++17
 Minor Release. Принят на днях.

---

# Компиляторы

 * **gcc** - основной компилятор на linux, самый старый, лицензия GPL.
 * **clang** - основной конкурент gcc, лицензия MIT привлекает компании. Основной компилятор на OSX и iOS.
 * **mvcc** - основной компилятор на Windows.
 * **icc** - компилятор от Intel, закрытый.

Компиляторы одновременно поддерживают несколько версий стандарта. `-std=c++14`

---

# Hello World

```c++
#include <iostream> // библиотека ввода - вывода

// Функция, с которой начинается исполнение программы.
int main() { 
    std::cout << "Hello World" << std::endl;
    return 0; // код возврата всей программы
}
```

---

# Объявление переменных

```
void f() {
//тип
// |     имя
// |      |     начальное значение
// |      |         |
  int my_variable = 0;
}
```

---

# Примитивные типы

```
#include <cstdint>

void f() {
  bool the_cake_is_a_lie = true;
  int answer_to_the_question = 42;
  size_t unsigned_number = 1U;
  uint64_t quite_big_number = -1ULL;

  float floating_point_number = 0.5;
  double double_precision_floating_point_number = 1e9;
}
```

---

# Сложные типы

```c++
#include <vector>
#include <string>
#include <map>

void f() {
  std::vector<int> array = { 1, 2, 3, 42 };

  std::string name = "Fedor";

  std::pair<std::string, std::string> full_name = {
    "Fedor", "Korotkiy"
  };

  std::map<std::string, int> scores = {
    { "Petya", 10 },
    { "Vasya", -1 }
  };
}
```

---

# Ввод/вывод `<iostream>`

```c++
void f() {
    int a, b;
    std::cin >> a >> b;
    std::cout << (a + b) << std::endl;
}
```

```
1 2
3
```

 * В серьёзном проекте скорее вы скорее всего будете использовать какую-то стороннюю библиотеку ввода-вывода.

---

# Логические операции

```c++
void f() {
  bool v = (!true || false) && (0 != 2);
  bool g = 1 > 15;

  // оптимизация логических выражений
  bool b = president_is_alive || launch_nukes();
}
```

---

# Функции

```
// Определение (definition)
int sum(int a, int b) {
  int c = a + b;
  return c;
}

// Объявление (declaration)
std::vector<int> reverse(std::vector<int> b);

void f() {
  std::cout << sum(1, 2) << std::endl;
  std::vector<int> x = { 1, 2, 3 };
  std::vector<int> y = reverse(x);
}
```

---

# auto

```
std::vector<int> reverse(std::vector<int> b);

void f() {
  std::vector<int> x = { 1, 2, 3 };

  auto y = reverse(x);
}
```

---

# Оператор if

```c++
void PrintSign(int i) {
  if (i > 0) {
    std::cout << "positive" << std::endl;
  } else if (i < 0) {
    std::cout << "negative" << std::endl;
  } else {
    std::cout << "zero" << std::endl;
  }
}
```

---

# Циклы

```c++
void PrintVector(std::vector<int> some_numbers) {
  for (size_t i = 0; i < some_numbers.size(); ++i) {
    std::cout << some_numbers[i] << std::endl;
  }
}

std::vector<int> Remove42Suffix(std::vector<int> v) {
  while (!v.empty() && v.back() == 42) {
      v.pop_back();
  }
  return v;
}
```

---

# Range-based for

```c++
void PrintVector(std::vector<int> some_numbers) {
  for (int number : some_numbers) {
    std::cout << number << std::endl;
  }
}
```

---

# `std::vector`

```c++
void f() {
  std::vector<int> years = { 1992, 2005, 2015 };
  years[0] = 1812;

  years.size() == 3;
  years[0] == 1812;
  years[years.size() - 1] == years.back();
  years.back() == 2015;

  years.push_back(2016); // { 1812, 2005, 2015, 2016 }
  years.size() == 4;
  years.back() == 2016;
  years.pop_back();
  years.resize(6); // { 1812, 2005, 2015, 0, 0, 0 }

  std::vector<std::vector<std::string>> = {
    { "a", "b" },
    { "c", "d" }
  };
}
```

---

# `std::map`

```c++
void f() {
  std::map<std::string, int> word_counts;
  word_counts["The"] = 10000;
  word_counts["a"] = 50;

  word_counts["a"] == 50;
  word_counts["b"] == 0; // ???

  // std::map<std::string, int>::iterator
  auto it = word_counts.find("c");
  if (it != word_counts.end()) {
    std::cout << it->first << " "
      << it->second << std::endl;
  }
  word_counts.erase("The");
  word_counts.size() == 2;
}
```

---

# `std::string`

```c++
void f() {
  std::string name = "Fedor";
  std::string nick = "prime";
  name.size() == 5;
  name[1] == 'e';

  std::string display_name = name + " aka " + nick;
}
```

---

# Что нужно запомнить

 - `std::vector`, `std::map`, `std::string`
 - `auto` - ваш друг
 - https://cppreference.com

---

# Сборка C++

