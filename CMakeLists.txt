project(shad-cpp0)

cmake_minimum_required(VERSION 3.8)

set(CMAKE_CXX_STANDARD             17)
set(CMAKE_MODULE_PATH              "${CMAKE_SOURCE_DIR}/cmake")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")
set(CMAKE_EXPORT_COMPILE_COMMANDS  ON)

find_package(Catch REQUIRED)
include(cmake/TestSolution.cmake)

add_subdirectory(bin-pow)
add_subdirectory(range)
add_subdirectory(unique)
add_subdirectory(split)
add_subdirectory(polish-notation)
add_subdirectory(word-count)
add_subdirectory(permutations)
add_subdirectory(reverse-map)
add_subdirectory(long-sum)
add_subdirectory(diff-pairs)

add_subdirectory(expr-parser)