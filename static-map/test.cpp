#include <testing.h>
#include <util.h>
#include <static_map.h>

namespace tests {

void TestEmptyMap() {
    StaticMap map({});

    std::string value;
    ASSERT_EQ(false, map.Find("key", &value));
}

void TestSmallMap() {
    StaticMap map({ { "b", "1" }, { "a", "2" } });

    std::string value;
    ASSERT_EQ(true, map.Find("b", &value));
    ASSERT_EQ("1", value);

    ASSERT_EQ(true, map.Find("a", &value));
    ASSERT_EQ("2", value);

    ASSERT_EQ(false, map.Find("c", &value));
    ASSERT_EQ(false, map.Find("0", &value));
}

void TestSpeed() {
    const int NUM_ELEMENTS = 100000;

    std::vector<std::pair<std::string, std::string>> items;
    for (int i = 0; i < NUM_ELEMENTS; ++i) {
        items.emplace_back(std::to_string(i), std::to_string(i));
    }

    RandomGenerator rnd(73467345);
    rnd.Shuffle(items.begin(), items.end());
    
    StaticMap map(items);

    for (int i = 0; i < NUM_ELEMENTS; ++i) {
        std::string value;
        ASSERT_EQ(true, map.Find(std::to_string(i), &value));
        ASSERT_EQ(std::to_string(i), value);
    }
}    

void TestAll() {
    StartTesting();
    RUN_TEST(TestEmptyMap);
    RUN_TEST(TestSmallMap);
    RUN_TEST(TestSpeed);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
