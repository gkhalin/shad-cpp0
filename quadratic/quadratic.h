#pragma once

#include <stdexcept>

enum RootsCount {
    ZERO,
    ONE,
    TWO,
    INF
};

struct Roots {
    RootsCount count;
    double first, second;
};

Roots SolveQuadratic(int a, int b, int c) {
    throw std::runtime_error("Not implemented");
}
