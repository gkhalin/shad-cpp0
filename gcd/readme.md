# GCD

Это задача типа [crashme](https://best-cpp-course-ever.ru/prime/shad-cpp/blob/master/crash_readme.md).

Исходный код находится в файле gcd.cpp. Исполяемый файл получен командой
```
g++ -std=c++14 -O0 -Wall gcd.cpp -o gcd
```
